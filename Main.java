package e2;

public class Main {

    public static void main(String[] args) {
        Integer monitor1 = new Integer(1);
        Integer monitor2 = new Integer(1);

        Thread2 f2 = new Thread2(monitor1, monitor2, 3, 6, 3);
        Thread1 f1 = new Thread1(monitor1, 2, 4, 2);
        Thread1 f3 = new Thread1(monitor2, 2, 5, 5);

        f2.start();
        f1.start();
        f3.start();
    }
}
